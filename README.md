#	Highmaps Widget

__INTRODUCTION__: This project is for a Sisense plugin, which generates a new widget type leveraging Highchart's HighMaps library.

![Map of Europe](screenshots/europe.png)

__IMPLEMENTATION__: 

*Step 1:* Download the attached .zip file, and extract the plugins folder to the following path "[Sisense installation path]\PrismWeb\Plugins". 

*Step 2:* Open the included file, *config.js*, and check out the maps variable.  This is a list of all available map types for this widget.  You can add to this list by going to Highchart's website and downloading the geo file of your choice.  Just download the javascript version of the map you want, place the js file in the plugin's maps folder, and update the config.js file to include a reference to your map.  You will also need to specify the following properties for your map:

* label: This is the label that shows up in the widget editor
* source: The name used by the Highmaps JS file to save the map.  This is usually found at the very beginning of the file where you see *Highcharts.maps["source"] = *
* key: This is the geojson property we will use to match Elasticube data to the map

http://code.highcharts.com/mapdata/

*Step 3:* On your dashboard, click the *Create Widget* button and select *Advanced Configuration*. Next, select *Route Map* from the dropdown.  In the data tab, there are several panels for adding data.

* Geography: This dimension will be used to match a data point from the Elasticube to a region on the map
* Label: The label to use for each region
* Values : One or more measures to calculate per point.  The first value added is also used to determine the coloring of the points

*Step 5:* Formatting

![Making a filter selection](screenshots/widget-editor.png)

* Map Type: Which geojson file should we use to create the map
* Axis Scale: Can use either a logarithmic or linear scale for coloring
* Axis Location: Where to place the axis scale
* Border Color: The color in between regions on the map
* Animation Speed: How quickly the map loads into view

__NOTES__: 
* This sample has been confirmed working on Sisense version 7.0.1, and should be backwards compatible with previous version.
* For more information about how HighMaps works, check out their documentation page at https://www.highcharts.com/docs/maps/map-collection
* There is a known limitation with this plugin, as it does not support export to PDF