
var settings = {
	'maps': [
		{
			'label': 'japan',
			'source': 'countries/jp/jp-all',
			'key': 'fips'	//	FIP code of each prefecture
		},{
			'label': 'europe',
			'source': 'custom/europe',
			'key': 'iso-a3'	//	3 character ISO country code
		},{
			'label': 'united kingdom',
			'source': 'countries/gb/custom/gb-countries',
			'key': 'hc-a2'	//	2 character country code (defined by highcharts)
		}	
	],
	'lowColor': '#F6F6F6'
}