mod.controller('stylerController', ['$scope',
    function ($scope) {

        /**
         * variables
         */

        //  Save the widget id (handle new widgets)
        $scope.widgetId = $scope.widget.oid ? $scope.widget.oid : 'new';

        /**
         * watches
         */
        $scope.$watch('widget', function (val) {

            //  Get a reference to the list of basemaps
            $scope.mapTypes = settings.maps;

            //  Set the model, from the style
            $scope.model = $$get($scope, 'widget.style');
        });

        /**
        * public methods
        */

        //  Function to simply redraw the widget
        $scope.redraw = function(){
            
            $scope.widget.redraw();
        }
        
        //  Generic function to set a style propery
        $scope.setStyle = function(prop, value){

            //apply changes
            $scope.widget.style[prop] = value;

            //  Draw the map again
            $scope.redraw();
        }
    }
]);