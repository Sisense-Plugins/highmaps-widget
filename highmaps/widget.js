
//  Panel name defintion
var panels = {
    "geo": "Geography",
    "label": "Label",
    "values": "Values"
}

//  New Chart type registration
prism.registerWidget("highmaps", {
    name: "highmaps",
    family: "map",
    title: "Highchart Map",
    iconSmall: "/plugins/highmaps/icon-24.png",
    styleEditorTemplate: "/plugins/highmaps/styler.html",
    hideNoResults: true,
    style: {
        mapType: "japan",
        axisScale: "linear",
        axisLocation: "left",
        animationSpeed: 1000,
        borderColor: "white",
        borderWidth: 0.2
    },
    data: {
        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name: panels.geo,
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: panels.label,
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: panels.values,
                type: "visible",
                metadata: {
                    types: ['measures'],
                    maxitems: 5
                },
                itemAttributes: ["color"],
                allowedColoringTypes: function(){
                    return {
                        color: true,
                        condition: false,
                        range: false
                    }
                },
                visibility: true
            },
            {
                name: 'filters',
                type: 'filters',
                metadata: {
                    types: ['dimensions'],
                    maxitems: -1
                }
            }
        ],
        //  Allow coloring for the value panel
        canColor: function (widget, panel, item) {
            return (panel.name === panels.values);
        },
        // builds a jaql query from the given widget
        buildQuery: function (widget) {

            // building jaql query object from widget metadata 
            var query = { 
                datasource: widget.datasource, 
                format: "json",
                isMaskedResult:true,
                metadata: [] 
            };

            //  Get the metadata panels
            var geoItems = widget.metadata.panel(panels.geo).items,
                labelItems = widget.metadata.panel(panels.label).items,
                valItems = widget.metadata.panel(panels.values).items,
                filterItems = widget.metadata.panel("filters").items;

            //  Do we have all required fields?
            var isValid = (geoItems.length == 1) && (valItems.length>0);
            if (isValid) {

                //  Add the geography to the query
                geoItems.forEach(function(item){

                    //  Clone the old object
                    var newItem = $$.object.clone(item, true);
                    
                    //  Look for any matching dashboard filters
                    var dashboardFilters = [];
                    widget.dashboard.filters.$$items.forEach(function(f){
                        if (f.isCascading){
                            f.levels.forEach(function(f2){
                                if(f2.dim == item.jaql.dim) {
                                    dashboardFilters = f2.filter.members;
                                }
                            })
                        } else if (f.jaql.dim == item.jaql.dim) {
                            dashboardFilters = f.jaql.filter.members;
                        }
                    })

                    //  Was there a selection?
                    if (dashboardFilters && dashboardFilters.length > 0) {

                        //  Create the filter base
                        newItem.jaql.in = {
                            'selected': {
                                'jaql': $$.object.clone(item.jaql, true)
                            }
                        }

                        //  Set the filter selection
                        newItem.jaql.in.selected.jaql.filter = {
                            'members': dashboardFilters
                        }
                    }

                    query.metadata.push(newItem);
                })
                
                //  Add the label (optional)
                labelItems.forEach(function(item){
                    query.metadata.push(item);
                })

                // push Value Items
                valItems.forEach(function(item){
                    query.metadata.push(item);
                })

                // push widget filter selections
                filterItems.forEach(function(item){
                    
                    //  Create a new version of the item, and specify it's a filter
                    var newItem = $$.object.clone(item, true);
                    newItem.panel = "scope";

                    //  Save to metadata array
                    query.metadata.push(newItem);
                })
            }
            
            return query;
        },
        //  Build geojson object from query result
        processResult : function (widget, queryResult) {

            //  Get the Sisense formatter, & check to see if the query result is empty
            var formatter = prism.$injector.get('$filter')('numeric'),
                hasData = queryResult.$$rows.length > 0;

            //  Get a default color just in case (first color from the palette)
            var defaultColor = prism.$ngscope.dashboard.style.options.palette.colors[0];

            //  Get the metadata panels
            var geoItems = widget.metadata.panel(panels.geo).items,
                labelItems = widget.metadata.panel(panels.label).items,
                valItems = widget.metadata.panel(panels.values).items,
                filterItems = widget.metadata.panel("filters").items;

            //  Is there a selected geo?
            var hasSelection = queryResult.metadata().filter(function(i) { 
                return (i.jaql.dim == $$get(geoItems, '0.jaql.dim',null)) && (i.jaql.in); 
            }).length > 0;

            //  Get a reference to the selected map type
            var mapType = settings.maps.filter(function(map){
                return map.label == widget.style.mapType;
            })[0];

            //  Find array of all geographic features for this map
            var features = $$get(Highcharts.maps, mapType.source + '.features', []);

            //  Check for how many items were specified
            var hasLabel = labelItems.length > 0,
                numValues = valItems.length,
                valuesIndex = hasLabel ? 2 : 1;
        
            //  Create an array to hold the data points
            var data = [];

            //  Create placeholders for min/max values
            var min = $$get(queryResult.$$rows, '0.' + valuesIndex + '.data', 0),
                max = $$get(queryResult.$$rows, '0.' + valuesIndex + '.data', 0);
            
            //  Loop through each route
            for (var i=0; i<queryResult.$$rows.length; i++) {
                
                //  Get the values from the row
                var geo = queryResult.$$rows[i][0].data,
                    label = hasLabel ? queryResult.$$rows[i][1].text : geo,
                    firstValue = $$get(queryResult.$$rows[i], valuesIndex + '.data', 0);

                //  Check to see if 
                var matchedFeature = features.filter(function(feature){ 
                    return feature.properties[mapType.key] == geo; 
                }).length > 0

                //  Only add this feature if we have a way to plot it on the map
                if (matchedFeature) {

                    //  Check for min/max
                    if (min > firstValue) {
                        min = firstValue
                    }
                    if (max < firstValue){
                        max = firstValue
                    }

                    //  Check to see if this specific geo was selected by a dashboard filter 
                    var isSelected = hasSelection ? $$get(queryResult.$$rows[i][1], 'selected', false) : true;
                        
                    //  Create a data point for this row of data
                    var point = {
                        "code": geo,
                        "name": label,
                        "isSelected": isSelected,
                        "value": firstValue,
                        "tooltips": {}
                    }

                    //  Save the values to the route's properties
                    for (var j=0; j<numValues; j++){

                        //  Get the value details
                        var valueKey = valItems[j].jaql.title,
                            valueData = queryResult.$$rows[i][valuesIndex+j].data,
                            valueMask = $$get(valItems[j],'format.mask', null),
                            valueColor = $$get(queryResult.$$rows[i][valuesIndex+j],'color', defaultColor);

                        //  Save all values to the data point
                        point.tooltips[valueKey] = {
                            'value': valueData,
                            'text': formatter(valueData,valueMask),
                            'color': valueColor ? valueColor : defaultColor
                        };
                    }

                    //  Save this data point to the array
                    data.push(point);
                }
            }

            //  Define an object containing the new results
            var newResults = {
                data: data,
                stats: {
                    min: min,
                    max: max
                }
            };
            
            //  if we have data, return the map data
            return hasData ? newResults : null;
        }
    },
    beforequery: function(widget,args){

        //  Get the route label's metadata item
        var geoItems = $$get(widget.metadata.panel(panels.label), 'items.0',null);
        if (geoItems){

            //  Filter out metadata items that are filters AND match the geo's dimension
            var newMeta = args.query.metadata.filter(function(item){
                var isMatch = (item.panel == "scope") && (item.jaql.dim == geoItems.jaql.dim);
                return !isMatch;
            })
            args.query.metadata = newMeta;
        }
    },
    render : function (widget, event) {

        //  Find the HTML container element
        var divId = "highmap-" + widget.oid;
        var container = prism.$ngscope.appstate == "widget" ? $('.widget-body') : $('.widget-body', $('widget[widgetid="' + widget.oid + '"]'));

        //  Figure out the height/width
        var height = container.height(),
            width = container.width();

        //  Clear any old contents
        container.empty();
        container.append("<div id='" + divId +  "' class='highmap-plugin'></div>");

        //  What map type was selected
        var mapType = settings.maps.filter(function(map){
            return map.label == widget.style.mapType;
        })[0];

        //  Get the metadata panels
        var geoItems = widget.metadata.panel(panels.geo).items,
            labelItems = widget.metadata.panel(panels.label).items,
            valItems = widget.metadata.panel(panels.values).items;
            
        //  Figure out the label to use
        var label = $$get(labelItems, '0.jaql.title', $$get(geoItems, '0.jaql.title', 'Geography')); 

        //  Figure out the colors to use
        var defaultColor = prism.$ngscope.dashboard.style.options.palette.colors[0];
        var maxColor = $$get(valItems, '0.format.color.color', defaultColor);

        //  Define the highcharts options
        var options = {
            chart: {
                map: mapType.source,
                height: height,
                width: width
            },
            exporting: {
                enabled: false
            },
            title: {
                text: ''
            },
            legend: {
                borderWidth: 0,
                floating: true,
            },
            mapNavigation: {
                enabled: false
            },
            colorAxis: {
                min: widget.queryResult.stats.min,
                max: widget.queryResult.stats.max,
                minColor: settings.lowColor,
                maxColor: maxColor,
                type: widget.style.axisScale
            },
            series: [{
                animation: {
                    duration: widget.style.animationSpeed
                },
                data: widget.queryResult.data,
                name: label,
                joinBy: [mapType.key, 'code'],
                borderColor: widget.style.borderColor,
                borderWidth: widget.style.borderWidth,
                states: {
                    hover: {
                        borderWidth: 1
                    }
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }]
        };

        //  Figure out how to display the axis legend
        if (widget.style.axisLocation == 'left') {
            options.legend.align = 'left';
            options.legend.layout = 'vertical';
        } else if (widget.style.axisLocation == 'right'){
            options.legend.align = 'right';
            options.legend.layout = 'vertical';
        } else if (widget.style.axisLocation == 'top'){
            options.legend.verticalAlign = 'top';
        } else {    // bottom
            options.legend.verticalAlign = 'bottom';
        }

        //  Render the map
        Highcharts.mapChart(divId, options);
    },
    destroy : function (widget, args) {},
    options: {
        dashboardFiltersMode: "slice",
        selector: false,
        title: false
    },
    sizing: {
        minHeight: 128, //header
        maxHeight: 2048,
        minWidth: 128,
        maxWidth: 2048,
        height: 320,
        defaultWidth: 512
    }
});